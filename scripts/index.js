'use strict';

let habits = [];
const HABIT_KEY = 'HABIT_KEY';
let gloabalActiveHabitId;

const page = {
    menu: document.querySelector('.menu__list'),
    header: {
        h1: document.querySelector('.h1'),
        progressPercent: document.querySelector('.progress__percent'),
        progressCoverBar: document.querySelector('.progress__cover-bar')
    },
    content: {
        daysContainer: document.getElementById('days'),
        nextDay: document.querySelector('.habit__day')
    },
    popup: {
        popupCover: document.querySelector('.cover'),
        popupIcon: document.querySelector('.popup__form input[name="icon"]')
    }
}

// Utils
function loadFromJSON(res) {
    localStorage.setItem(HABIT_KEY, JSON.stringify(res));
}

function saveData() {
    localStorage.setItem(HABIT_KEY, JSON.stringify(habits));
}

function loadData() {
    const habitsString = localStorage.getItem(HABIT_KEY);
    const habitArray = JSON.parse(habitsString);
    if (Array.isArray(habitArray)) {
        habits = habitArray;
        console.log(habits)
    }
}

if(localStorage.getItem(HABIT_KEY) === null) {
    fetch('./data/demo.json')
        .then((res) => res.json())
        .then((res) => loadFromJSON(res))
}

function resetFrom(form, fields) {
    for (const field of fields) {
        form[field].value = '';
    }
}

function falidateGetFormData(form, fields) {
    const data = new FormData(form);
    let res = {};
    for (const field of fields) {
        const fieldValue = data.get(field);
        form[field].classList.remove('error');
        if(!fieldValue) {
            form[field].classList.add('error'); 
        } 
        res[field] = fieldValue;
    }
    let isValid = true;
    for (const field of fields) {
        if (!res[field]) {
            isValid = false;
        }
    }
    if (!isValid) {
        return;
    }
    return res;
}

// render
function rerenderMennu(activeHabit) {
    for(const habit of habits) {
        const existed = document.querySelector(`[menu-habit-id="${habit.id}"]`);
        if (!existed) {
            const element = document.createElement('button');
            element.setAttribute('menu-habit-id', habit.id);
            element.classList.add('menu__item');
            element.addEventListener('click', () => {
                rerender(habit.id);
            })
            element.innerHTML = `<img src="./images/${habit.icon}.svg" alt="${habit.name}">`
            if (activeHabit.id === habit.id) {
                element.classList.add('menu__item_active');
            }
            page.menu.appendChild(element);
            continue;
        }
        if (activeHabit.id === habit.id) {
            existed.classList.add('menu__item_active');
        } else {
            existed.classList.remove('menu__item_active');
        }
    }
}

function renderHead(activeHabit) {
    page.header.h1.innerText = activeHabit.name;
    const progress = activeHabit.days.length / activeHabit.target > 1
        ? 100
        : activeHabit.days.length / activeHabit.target * 100;
    page.header.progressPercent.innerText = progress.toFixed(0) + "%";
    page.header.progressCoverBar.setAttribute('style', `width: ${progress}%`)
}

function renderBody(activeHabit) {
    page.content.daysContainer.innerHTML = '';
    for (let index in activeHabit.days) {
        const element = document.createElement('div');
        element.classList.add('habbit');
        element.innerHTML = `<div class="habit">
                <div class="habit__day">Day ${Number(index) + 1}</div>
                <div class="habit__coment">${activeHabit.days[index].comment}</div>
                <button class="habit__delete" onclick="deleteDay(${[index]})">
                    <img src="images/shape.svg" alt="delete day ${index + 1}">
                </button>
            </div>`;
        page.content.daysContainer.appendChild(element)
    }
    page.content.nextDay.innerHTML = `day ${activeHabit.days.length + 1}`;
}

function rerender(activeHabitId) {
    gloabalActiveHabitId = activeHabitId;
    const activeHabit = habits.find(habbit => habbit.id === activeHabitId)
    if(!activeHabit) {
        return;
    }
    document.location.replace(document.location.pathname + '#' + activeHabitId);
    rerenderMennu(activeHabit)
    renderHead(activeHabit)
    renderBody(activeHabit)
}

document.querySelector('.button').style.cssText = `pointer-events: none`;
document.querySelector('.input_icon').addEventListener('input', () => {
    if (document.querySelector('.input_icon').value != '')
    document.querySelector('.button').style.cssText = `pointer-events: visible`;
})

function addDays(event) {
    event.preventDefault();
    const data = falidateGetFormData(event.target, ['comment']);
    if (!data) {
        return;
    }
    const form = event.target;
    habits = habits.map(habit => {
        if(habit.id === gloabalActiveHabitId) {
            return {
                ...habit,
                days: habit.days.concat([{comment: data.comment}])
            }
        }
        return habit;
    })
    resetFrom(event.target, ['comment']);
    rerender(gloabalActiveHabitId);
    saveData()
    document.querySelector('.button').style.cssText = `pointer-events: none`;
}

function deleteDay(index) {
    habits = habits.map(habit => {
        if (habit.id === gloabalActiveHabitId) {
            habit.days.splice(index, 1);
            return {
                ...habit,
                days: habit.days
            }
        }
        return habit;
    })
    rerender(gloabalActiveHabitId);
    saveData()
}

function showPopup(event) {
    page.popup.popupCover.classList.remove('cover_hidden');
}
function closePopup(event) {
    page.popup.popupCover.classList.add('cover_hidden');
}
window.addEventListener('keydown', (e) => {
    if(e.key === 'Escape') {
        console.log(e)
        if(document.querySelector('.cover').classList.contains('cover_hidden')) {
            return;
        };
        document.querySelector('.cover').classList.add('cover_hidden');
    }
});

function setIcon(context, icon) {
    page.popup.popupIcon.value = icon;
    const activeIcon = document.querySelector('.icon.icon_active');
    activeIcon.classList.remove('icon_active');
    context.classList.add('icon_active');
}

function addHabit(event) {
    event.preventDefault();
    event.preventDefault();
    const data = falidateGetFormData(event.target, ['name', 'icon', 'target']);
    if (!data) {
        return;
    }
    const maxId = habits.reduce((acc,habit) => acc > habit.id ? acc : habit.id, 0)
    habits.push({
        id:maxId + 1,
        name: data.name,
        target: data.target,
        icon: data.icon,
        days: []
    })
    resetFrom(event.target, ['name', 'target']);
    closePopup()
    saveData()
    rerender(maxId + 1);
}

(() => {
    loadData(); 
    const hasId = Number(document.location.hash.replace('#', ''));
    const urlId = habits.find(habit => habit.id == hasId);
    if (urlId) {
        rerender(urlId.id);
    } else {
        rerender(habits[0].id);
    }
})()


const arr = [1,'3',1,2,2, 'asdasd', '213', true, 1];

let b = arr.filter((el, val,arr) => {
    return arr.indexOf(el) === val
})
console.log(b)

